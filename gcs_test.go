package storage_firebase

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewGCS(t *testing.T) {
	t.Run("creates a new storage client", func(t *testing.T) {
		client, err := NewGCS()

		assert.Nil(t, err)
		assert.NotNil(t, client)

		fmt.Println("client", client)
	})
}

func TestUploadImage(t *testing.T) {
	t.Run("uploads an image to a bucket", func(t *testing.T) {
		client, err := NewGCS()
		assert.Nil(t, err)

		bucketName := "privy-acceleration"
		objectName := "gcs_upload.png"
		filePath := "files/gcs_upload.png"

		err = UploadImage(client, bucketName, objectName, filePath)

		assert.Nil(t, err)
	})
}
