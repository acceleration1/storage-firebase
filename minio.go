package storage_firebase

import (
	"github.com/minio/minio-go"
	"storage-firebase/config"
)

// Minio is a struct
type Minio struct {
	client *minio.Client
	bucket string
}

// NewMinio is a constructor
func NewMinio(cfg *config.Config) (*Minio, error) {
	client, err := minio.New(
		cfg.StorageConfig.StorageEndpoint,
		cfg.StorageConfig.StorageAccessKeyID,
		cfg.StorageConfig.StorageAccessKeySecret,
		false,
	)
	if err != nil {
		return nil, err
	}

	client.SetAppInfo(cfg.AppName, "0.0.1")

	m := &Minio{
		client: client,
		bucket: cfg.StorageConfig.StorageBucketName,
	}

	return m, nil
}
