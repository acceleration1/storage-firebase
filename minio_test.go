package storage_firebase

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"os"
	"storage-firebase/config"
	"testing"
)

func TestNewMinio(t *testing.T) {
	err := godotenv.Load(".env")
	if err != nil {
		t.Fatal("Error loading .env file")
	}

	t.Run("returns a new Minio object when given valid config", func(t *testing.T) {
		cfg := &config.Config{
			StorageConfig: config.StorageConfig{
				StorageEndpoint:        os.Getenv("STORAGE_ENDPOINT"),
				StorageAccessKeyID:     os.Getenv("STORAGE_ACCESS_KEY_ID"),
				StorageAccessKeySecret: os.Getenv("STORAGE_ACCESS_KEY_SECRET"),
				StorageBucketName:      os.Getenv("STORAGE_BUCKET_NAME"),
			},
			AppName: "testApp",
		}

		m, err := NewMinio(cfg)

		assert.Nil(t, err)
		assert.NotNil(t, m)
		assert.NotNil(t, m.client)
		assert.Equal(t, os.Getenv("STORAGE_BUCKET_NAME"), m.bucket)

		fmt.Println("bucket_name :", m.bucket)
	})

	t.Run("returns error when Minio client cannot be created", func(t *testing.T) {
		cfg := &config.Config{
			StorageConfig: config.StorageConfig{
				StorageEndpoint:        "",
				StorageAccessKeyID:     os.Getenv("STORAGE_ACCESS_KEY_ID"),
				StorageAccessKeySecret: os.Getenv("STORAGE_ACCESS_KEY_SECRET"),
				StorageBucketName:      os.Getenv("STORAGE_BUCKET_NAME"),
			},
			AppName: "testApp",
		}

		m, err := NewMinio(cfg)

		assert.NotNil(t, err)
		assert.Nil(t, m)
	})
}
