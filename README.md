# # Privy Acceleration 12 - Storage and Firebase


## MINIO
### 1. You can define a new container for minio server using this command
``` bash
$ docker run -p 9000:9000 -p 9001:9001 quay.io/minio/minio server /data --console-address ":9001" 

# bind mounted volume on host, make sure you create a directory in the host
$ docker run -v /path/to:/data -p 9000:9000 -p 9001:9001 --name minio_server quay.io/minio/minio server /data --console-address ":9001"

# Note: /path/to is the host directory
# example on mac: /Users/john/minio/data 
 ```

### 2. Define .env file based on cfg {Storage Config Section} file to connect to minio server
#

## GCS
### 1. Add credential file in the secret directory
### 2. Add a image file in the files directory