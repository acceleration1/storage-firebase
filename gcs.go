package storage_firebase

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"google.golang.org/api/option"
	"io"
	"log"
	"os"
)

func NewGCS() (*storage.Client, error) {
	var (
		ctx        = context.Background()
		secretPath = "secrets/privyacceleration-fellowship.json"
	)

	client, err := storage.NewClient(ctx, option.WithCredentialsFile(secretPath))
	if err != nil {
		return nil, fmt.Errorf("storage.NewClient: %v", err)
	}
	return client, nil
}

func UploadImage(client *storage.Client, bucketName, objectName, filePath string) error {
	ctx := context.Background()

	bucket := client.Bucket(bucketName)

	wc := bucket.Object(objectName).NewWriter(ctx)
	wc.ContentType = "image/png"

	file, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("opening file gor err : %v", err)
	}
	defer file.Close()

	if _, err = io.Copy(wc, file); err != nil {
		return fmt.Errorf(": %v", err)
	}
	if err := wc.Close(); err != nil {
		return fmt.Errorf("Writer.Close: %v", err)
	}

	log.Printf("Image success uploaded to gs://%s/%s\n", bucketName, objectName)
	return nil
}
